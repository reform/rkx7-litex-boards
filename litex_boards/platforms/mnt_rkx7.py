#
# This file is part of LiteX-Boards.
#
# Copyright (c) 2021 Florent Kermarrec <florent@enjoy-digital.fr>
# Copyright (c) 2021 Lukas F. Hartmann, MNT Research GmbH <lukas@mntre.com>
# SPDX-License-Identifier: BSD-2-Clause

from litex.build.generic_platform import *
from litex.build.xilinx import XilinxPlatform
from litex.build.openocd import OpenOCD

# IOs ----------------------------------------------------------------------------------------------

_io = [
    # Clk / Rst.
    ("clk100", 0, Pins("AA10"), IOStandard("LVCMOS15")),

    # Serial.
    ("serial", 0,
        Subsignal("tx", Pins("D15")),
        Subsignal("rx", Pins("C18")),
        IOStandard("LVCMOS33")
    ),

    ("serial", 1,
        Subsignal("tx", Pins("H16")),
        Subsignal("rx", Pins("G16")),
        IOStandard("LVCMOS33")
    ),

    ("litescope_serial", 0,
        Subsignal("tx", Pins("C17")),
        Subsignal("rx", Pins("C16")),
        IOStandard("LVCMOS33")
    ),

    # SPIFlash
    ("spiflash4x", 0,  # clock needs to be accessed through STARTUPE2
        Subsignal("cs_n", Pins("C23")),
        Subsignal("dq",   Pins("B24 A25 B22 A22")),
        IOStandard("LVCMOS33")
    ),

    # SDCard.
    ("spisdcard", 0,
        Subsignal("clk",  Pins("C11")),
        Subsignal("mosi", Pins("A15"), Misc("PULLUP True")),
        Subsignal("cs_n", Pins("B15"), Misc("PULLUP True")),
        Subsignal("miso", Pins("A14"), Misc("PULLUP True")),
        Misc("SLEW=FAST"),
        IOStandard("LVCMOS18"),
    ),
    ("sdcard", 0,
        Subsignal("data", Pins("A14 B10 A12 B15"), Misc("PULLUP True")),
        Subsignal("cmd",  Pins("A15"), Misc("PULLUP True")),
        Subsignal("clk",  Pins("C11")),
        Subsignal("cd",   Pins("A10")),
        Misc("SLEW=FAST"),
        IOStandard("LVCMOS18"),
    ),

    # RGMII Ethernet
    ("eth_refclk", 0, Pins("F17"), IOStandard("LVCMOS33")), # CHECKME: Drive it?
    ("eth_clocks", 0,
        Subsignal("tx", Pins("E18")),
        Subsignal("rx", Pins("D18")),
        IOStandard("LVCMOS33")
    ),
    ("eth", 0,
        Subsignal("rst_n",   Pins("G17"), IOStandard("LVCMOS33")),
        Subsignal("int_n",   Pins("E16"), IOStandard("LVCMOS33")),
        Subsignal("mdio",    Pins("E15"), IOStandard("LVCMOS33")),
        Subsignal("mdc",     Pins("E17"), IOStandard("LVCMOS33")),
        Subsignal("rx_ctl",  Pins("F15"), IOStandard("LVCMOS33")),
        Subsignal("rx_data", Pins("J15 J16 F20 D20"), IOStandard("LVCMOS33")),
        Subsignal("tx_ctl",  Pins("D19"), IOStandard("LVCMOS33")),
        Subsignal("tx_data", Pins("H18 H17 G19 F18"), IOStandard("LVCMOS33")),
    ),

    # I2C
    ("i2c", 0,
        Subsignal("scl", Pins("Y26")),
        Subsignal("sda", Pins("W26")),
        IOStandard("LVCMOS18"),
    ),

    ("i2c", 1,
        Subsignal("scl", Pins("G12")),
        Subsignal("sda", Pins("A13")),
        IOStandard("LVCMOS18"),
    ),

    ("i2c", 2,
        Subsignal("scl", Pins("H26")),
        Subsignal("sda", Pins("G26")),
        IOStandard("LVCMOS33"),
    ),

    # GPIO
    ("gpio", 0,
        Pins("C13 C14 D13 AB26"), # hdmi_rst_n, edp_reset_n, usbhub_reset_n, dsi_rst_n
        IOStandard("LVCMOS18")
    ),

    # DDR3 SDRAM.
    ("ddram", 0,
        Subsignal("a",       Pins(
            "AC8 AA7 AA8 AF7 AE7 AC11 V9 Y10",
            "AB11 Y7  Y8 V11  V8  W11 Y11 V7 "),
            IOStandard("SSTL15")),
        Subsignal("ba",      Pins("AC7 AB7 AB9"), IOStandard("SSTL15")),
        Subsignal("ras_n",   Pins("AA9"), IOStandard("SSTL15")),
        Subsignal("cas_n",   Pins("AD8"), IOStandard("SSTL15")),
        Subsignal("we_n",    Pins("AC9"), IOStandard("SSTL15")),
        Subsignal("cs_n",    Pins("AD9"), IOStandard("SSTL15")),
        Subsignal("dm",      Pins(
            "U6 Y3 AB6 AD4"),
            IOStandard("SSTL15")),
        Subsignal("dq",      Pins(
            " V4  W3  U5  U1  U7  U2  V6  V3",
            " Y2  Y1 AA3  V2 AC2  W1 AB2  V1"),
            #"AA4 AB4 AC4 AC3 AC6  Y6  Y5 AD6",
            #"AD1 AE1 AE3 AE2 AE6 AE5 AF3 AF2"),
            #"AE5 AE1 AE3 AE2 AE6 AD1 AF3 AF2"), # with d24/d29 swapped
            IOStandard("SSTL15_T_DCI")),
        Subsignal("dqs_p",   Pins("W6 AB1 AA5 AF5"),
            IOStandard("DIFF_SSTL15")),
        Subsignal("dqs_n",   Pins("W5 AC1 AB5 AF4"),
            IOStandard("DIFF_SSTL15")),
        Subsignal("clk_p",   Pins("W10"),  IOStandard("DIFF_SSTL15")),
        Subsignal("clk_n",   Pins("W9"),   IOStandard("DIFF_SSTL15")),
        Subsignal("cke",     Pins("AB12"), IOStandard("SSTL15")),
        Subsignal("odt",     Pins("AC12"), IOStandard("SSTL15")),
        Subsignal("reset_n", Pins("AA2"),  IOStandard("LVCMOS15")),
        Misc("SLEW=FAST"),
        Misc("VCCAUX_IO=HIGH")
    ),

    # HDMI
    ("hdmi", 0,
        Subsignal("clk",     Pins("C12")),
        Subsignal("de",      Pins("D10")),
        Subsignal("hsync_n", Pins("D11")),
        Subsignal("vsync_n", Pins("E11")),
        Subsignal("b", Pins("H13 G10 J13 H12 J10  H8  H9 J11")), # [23:16]
        Subsignal("g", Pins("F14  C9 G14 F10 H14 G11 H11  G9")), # [15:8]
        Subsignal("r", Pins("E10  D8  F9  F8  A9  A8  B9  D9")), # [7:0]
        IOStandard("LVCMOS18")
    ),

    # MIPI-DSI
    ("dsi", 0,
        Subsignal("refclk",  Pins("AD18")),
        Subsignal("pclk",    Pins("AC18")),
        Subsignal("de",      Pins("AA15")),
        Subsignal("hsync_n", Pins("AB15")),
        Subsignal("vsync_n", Pins("AB16")),
        IOStandard("LVCMOS18")
    ),

    # USB (TUSB1310ZAY)
    ("usb_clkout", 0, Pins("Y23"), IOStandard("LVCMOS18")),
    ("usb_reset_n", 0, Pins("AF25"), IOStandard("LVCMOS18")),
    ("usb_enable", 0, Pins("AD25"), IOStandard("LVCMOS18")),
    ("usb_ulpi", 0,
        Subsignal("clk", Pins("AF22")),
        Subsignal("data", Pins(
            "AC21 AD21 AB21 Y20 W20 U17 T17 R16")),
        Subsignal("dir", Pins("AD24")),
        Subsignal("stp", Pins("Y21")),
        Subsignal("nxt", Pins("AE21")),
        IOStandard("LVCMOS18"),
    ),
    ("usb_pipe_ctrl", 0,
        Subsignal("phy_reset_n", Pins("P23")),
        Subsignal("tx_detrx_lpbk", Pins("AB22")),
        Subsignal("tx_elecidle", Pins("N24")),
        Subsignal("power_down", Pins("N23", "AD26")),
        #Subsignal("tx_oneszeros", Pins("")),
        #Subsignal("tx_deemph", Pins("", "")),
        #Subsignal("tx_margin", Pins("", "", "")),
        #Subsignal("tx_swing", Pins("")),
        #Subsignal("rx_polarity", Pins("")),
        #Subsignal("rx_termination", Pins("")),
        #Subsignal("rate", Pins("")),
        #Subsignal("elas_buf_mode", Pins("")),
        IOStandard("LVCMOS18"),
    ),
    ("usb_pipe_status", 0,
        Subsignal("rx_elecidle", Pins("AE23")),
        Subsignal("rx_status", Pins("M24", "N17", "P16")),
        Subsignal("phy_status", Pins("T22")),
        Subsignal("pwr_present", Pins("AE22")),
        IOStandard("LVCMOS18"),
    ),
    ("usb_pipe_data", 0,
        Subsignal("rx_clk", Pins("R21")),
        Subsignal("rx_valid", Pins("R26")),
        Subsignal("rx_data", Pins(
            "R20 M25 M26 R18 T23 T25 N19 P18  N18 T19 L24 K25 AE26 AE25 AF23 AF24")),
        Subsignal("rx_datak", Pins("K26", "L25")),

        Subsignal("tx_clk", Pins("N21")),
        Subsignal("tx_data", Pins(
            "R25 P26 T18 R17 U19 T20 R23 P25  R22 P24 N26 U20 M20 M19 M22 AD23")),
        Subsignal("tx_datak", Pins("M21", "AC22")),
        IOStandard("LVCMOS18"),
    ),

]

# Connectors ---------------------------------------------------------------------------------------

_connectors = []

# Platform -----------------------------------------------------------------------------------------

class Platform(XilinxPlatform):
    default_clk_name   = "clk100"
    default_clk_period = 1e9/100e6

    def __init__(self):
        XilinxPlatform.__init__(self, "xc7k325t-ffg676-2", _io, _connectors, toolchain="vivado")

    def create_programmer(self):
        return OpenOCD("openocd_xc7_ft2232.cfg", "bscan_spi_xc7a325t.bit")

    def do_finalize(self, fragment):
        XilinxPlatform.do_finalize(self, fragment)
        self.add_period_constraint(self.lookup_request("clk100", loose=True), 1e9/100e6)
        self.add_platform_command("set_property INTERNAL_VREF 0.750 [get_iobanks 33]")
        self.add_platform_command("set_property INTERNAL_VREF 0.750 [get_iobanks 34]")
